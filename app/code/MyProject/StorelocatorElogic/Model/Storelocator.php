<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Model;

use Magento\Framework\Model\AbstractModel;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;

/**
 * StorelocatorElogic Storelocator Model
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @since 100.0.2
 */
class Storelocator extends AbstractModel implements StorelocatorInterface
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'storelocatorelogic_storelocator';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator::class);
    }

    /**
     * Get ID
     *
     * @return string|null
     */
    public function getId(): ?string
    {
        return parent::getData(self::STORELOCATOR_ID);
    }

    /**
     * Get name store
     *
     * @return string|null
     */
    public function getNameStore(): ?string
    {
        return parent::getData(self::NAME_STORE);
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * Get images
     *
     * @return string|null
     */
    public function getImages(): ?string
    {
        return parent::getData(self::IMAGES);
    }

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return parent::getData(self::ADDRESS);
    }

    /**
     * Get url key
     *
     * @return string|null
     */
    public function getUrlKey(): ?string
    {
        return parent::getData(self::URL_KEY);
    }

    /**
     * Get work schedule
     *
     * @return string|null
     */
    public function getWorkSchedule(): ?string
    {
        return parent::getData(self::WORK_SCHEDULE);
    }

    /**
     * Get longitude
     *
     * @return string|null
     */
    public function getLongitude(): ?string
    {
        return parent::getData(self::LONGITUDE);
    }

    /**
     * Get latitude
     *
     * @return string|null
     */
    public function getLatitude(): ?string
    {
        return parent::getData(self::LATITUDE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return StorelocatorInterface
     */
    public function setId($id): StorelocatorInterface
    {
        return $this->setData(self::STORELOCATOR_ID, $id);
    }

    /**
     * Set name store
     *
     * @param $nameStore
     * @return StorelocatorInterface
     */
    public function setNameStore($nameStore): StorelocatorInterface
    {
        return $this->setData(self::NAME_STORE, $nameStore);
    }

    /**
     * Set description
     *
     * @param $description
     * @return StorelocatorInterface
     */
    public function setDescription($description): StorelocatorInterface
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Set images
     *
     * @param $images
     * @return StorelocatorInterface
     */
    public function setImages($images): StorelocatorInterface
    {
        return $this->setData(self::IMAGES, $images);
    }

    /**
     * Set address
     *
     * @param $address
     * @return StorelocatorInterface
     */
    public function setAddress($address): StorelocatorInterface
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Set url key
     *
     * @param $url_key
     * @return StorelocatorInterface
     */
    public function setUrlKey($url_key): StorelocatorInterface
    {
        return $this->setData(self::URL_KEY, $url_key);
    }

    /**
     * Set work schedule
     *
     * @param $workSchedule
     * @return StorelocatorInterface
     */
    public function setWorkSchedule($workSchedule): StorelocatorInterface
    {
        return $this->setData(self::WORK_SCHEDULE, $workSchedule);
    }

    /**
     * Set longitude
     *
     * @param $longitude
     * @return StorelocatorInterface
     */
    public function setLongitude($longitude): StorelocatorInterface
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * Set latitude
     *
     * @param $latitude
     * @return StorelocatorInterface
     */
    public function setLatitude($latitude): StorelocatorInterface
    {
        return $this->setData(self::LATITUDE, $latitude);
    }
}
