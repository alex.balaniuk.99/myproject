<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Plugin;

use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface;
use MyProject\StorelocatorElogic\Model\StorelocatorRepository;

/**
 * Class UrlRewriteDeletePlugin
 * @package MyProject\StorelocatorElogic\Plugin
 */
class UrlRewriteDeletePlugin
{
    /**
     * @var UrlRewriteCollectionFactory
     */
    protected $urlRewriteCollectionFactory;

    /**
     * UrlRewriteDeletePlugin constructor.
     * @param UrlRewriteCollectionFactory $urlRewriteCollectionFactory
     */
    public function __construct(
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory
    ) {
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
    }

    /**
     * @param StorelocatorRepository $subject
     * @param StorelocatorInterface $storelocator
     * @return void
     */
    public function beforeDelete(StorelocatorRepository $subject, StorelocatorInterface $storelocator): void
    {
        $targetPath = ('storelocator/store/index/id/' . $storelocator->getId() . '/');
        $collection = $this->urlRewriteCollectionFactory->create();
        $collection->addFieldToFilter("target_path", ["eq"=>$targetPath]);
        $collection->getSelect();
        $collection->walk('delete');
    }
}
