<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\Collection;
use MyProject\StorelocatorElogic\Model\ResourceModel\Storelocator\CollectionFactory;

class Index extends Template
{
    const PAGER = [10 => 10];

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\UrlRewrite\Model\UrlRewriteFactory
     */
    protected $_urlRewriteFactory;

    /**
     * Storelocator constructor.
     * @param CollectionFactory $collectionFactory
     * @param Template\Context $context
     * @param UrlInterface $urlBuilder
     * @param RequestInterface $request
     * @param array $data
     * @param \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Template\Context $context,
        UrlInterface $urlBuilder,
        RequestInterface $request,
        \Magento\UrlRewrite\Model\UrlRewriteFactory $urlRewriteFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->collectionFactory = $collectionFactory;
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->_urlRewriteFactory = $urlRewriteFactory;
    }

    /**
     * @return $this|Index
     * @throws LocalizedException
     */
    protected function _prepareLayout(): Index
    {
        parent::_prepareLayout();

        if ($this->getStores()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'pager'
            )->setAvailableLimit(self::PAGER)->setShowPerPage(true)->setCollection(
                $this->getStores()
            );
            $this->setChild('pager', $pager);
            $this->getStores()->load();
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml(): string
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return object
     */
    public function getStores(): object
    {
        $page=($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $collection = $this->collectionFactory->create();

        $request = $this->request->getParam('name_store');
        if (!empty($request)) {
            $collection->addFieldToFilter('name_store', ['like' => '%' . $request . '%']);
        }

        $collection->setOrder('storelocator_id', 'ASC');
        $collection->setPageSize(10);
        $collection->setCurPage($page);
        return $collection;
    }

    /**
     * @return string
     */
    public function getLocatorUrl(): string
    {
        return $this->urlBuilder->getUrl('storelocator');
    }

    /**
     * @param $url_key
     * @return string
     */
    public function getStoreIdRewrite($url_key): string
    {
        return $this->urlBuilder->getUrl('store/' . $url_key);
    }
}
