<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MyProject\StorelocatorElogic\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 * @api
 * @since 100.0.2
 */
interface StorelocatorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \MyProject\StorelocatorElogic\Api\Data\StorelocatorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
