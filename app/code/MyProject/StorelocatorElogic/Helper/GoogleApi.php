<?php
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Helper;

use Magento\Framework\Exception\CouldNotSaveException;
use Zend_Http_Client;

/**
 * Class GoogleApi
 * @package MyProject\StorelocatorElogic\Helper
 */
class GoogleApi
{
    const GOOGLE_MAPS_HOST = 'https://maps.googleapis.com/maps/api/geocode/';

    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * @var Zend_Http_Client
     */

    /**
     * @var Zend_Http_Client
     */
    protected $client;

    /**
     * GoogleApi constructor.
     * @param Data $dataHelper
     * @param Zend_Http_Client $client
     */
    public function __construct(
        Data $dataHelper,
        Zend_Http_Client $client
    ) {
        $this->dataHelper = $dataHelper;
        $this->client = $client;
    }

    /**
     * @param string $address
     * @return array
     * @throws CouldNotSaveException
     */
    public function getGoogleApi(string $address): array
    {
        try {
            $this->client->setUri(self::GOOGLE_MAPS_HOST . 'json');
            $this->client->setMethod(Zend_Http_Client::GET);
            $this->client->setParameterGet('address', $address);
            $this->client->setParameterGet('key', $this->dataHelper->getApiKey());
            $response = $this->client->request();
        } catch (\Zend_Http_Client_Exception $e) {
            throw new CouldNotSaveException(__('Request error'));
        }
        if ($response->isSuccessful() && $response->getStatus() == 200) {
            try {
                $response = json_decode($response->getBody())->results[0];
                $location = $response->geometry->location;
                return [
                    'latitude' => $location->lat,
                    'longitude' => $location->lng
                ];
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__('Invalid address'));
            }
        } else {
            throw new CouldNotSaveException(__('Google Maps API error'));
        }
    }
}
