<?php
/**
 * ImportExport MySQL resource helper model
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Helper;

use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class GetAutoIncremet
 * @package MyProject\StorelocatorElogic\Helper
 */
class GetAutoIncremet extends \Magento\Framework\DB\Helper
{
    /**
     * GetAutoIncremet constructor.
     * @param ResourceConnection $resource
     * @param string $modulePrefix
     */
    public function __construct(ResourceConnection $resource, $modulePrefix = 'storelocatorelogic')
    {
        parent::__construct($resource, $modulePrefix);
    }

    /**
     * Returns next autoincrement value for a table
     *
     * @param string $tableName Real table name in DB
     * @return string
     * @throws LocalizedException
     */
    public function getNextAutoincrement(string $tableName): string
    {
        $connection = $this->getConnection();
        $entityStatus = $connection->showTableStatus($tableName);

        if (empty($entityStatus['Auto_increment'])) {
            throw new LocalizedException(__('Cannot get autoincrement value'));
        }
        return $entityStatus['Auto_increment'];
    }
}
