<?php

declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator;

/**
 * Class Index
 * @package MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator
 */
class Index extends Storelocator
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('MyProject_StorelocatorElogic::menu');
        $resultPage->getConfig()->getTitle()->prepend((__('Stores')));

        return $resultPage;
    }
}
