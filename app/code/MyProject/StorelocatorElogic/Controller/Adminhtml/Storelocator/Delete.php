<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator;

use MyProject\StorelocatorElogic\Api\StorelocatorRepositoryInterface;
use Magento\Backend\App\Action;
/**
 * Class Delete
 * @package MyProject\StorelocatorElogic\Controller\Adminhtml\Storelocator
 */
class Delete extends Storelocator
{
    /**
     * @var StorelocatorRepositoryInterface
     */
    protected $storelocatorRepositoryInterface;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param StorelocatorRepositoryInterface $storelocatorRepository
     */
    public function __construct(
        Action\Context $context,
        StorelocatorRepositoryInterface $storelocatorRepository
    ) {
        $this->storelocatorRepositoryInterface = $storelocatorRepository;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute(): \Magento\Framework\Controller\ResultInterface
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('storelocator_id');
        if ($id) {
            try {
                // init model and delete
                $this->storelocatorRepositoryInterface->deleteById($id);
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the store.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['storelocator_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a store to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
